# uniMixin

> uni-app&web

* vue项目中常规请求的混入插件。
* vue3项目中支持Composition API。

> 注意

* Internet Explorer和较老的浏览器中不支持，所以请谨慎使用。
* 默认使用uni.request请求方式，其他方式请在全局定义中的useRequest中自行定义。

> 喜欢组合式API的可以使用[request](https://gitee.com/xy_zi/request)

# 插件引入

* NPM

```
npm install @xyzi/unimixin
```

* main.js

```js
import uniMixin from "@xyzi/unimixin";

Vue.use(uniMixin);
```

# Option API
## 简单使用
### get请求

```js
export default {
	data(){
		return {
			/**** 默认生成data *****/
			getState: {
				path: undefined,  // 必填项，请求路径
				params: undefined,// 选填，请求参数
				data: undefined,  // 选填，请求结果
				loading: false,   // 不填，请求状态
				request: undefined// 不填，请求实例
			}
		}
	},
	methods:{
		/**** 默认生成请求方法 *****/
		getRequest(params){},     // method: "GET"
		/**** 选填方法 *****/
		getBefore(params){}, // 请求前
		getAfter(params){},  // 请求后
		getFinish(params){}, // 请求完成
	},
	uniMixin: "get",
}
```

### post请求

```js
export default {
	data(){
		return {
			/**** 默认生成data *****/
			postState: {
				path: undefined,  // 必填项，请求路径
				params: undefined,// 选填，请求参数
				data: undefined,  // 选填，请求结果
				loading: false,   // 不填，请求状态
				request: undefined// 不填，请求实例
			}
		}
	},
	methods:{
		/**** 默认生成请求方法 *****/
		postRequest(params){},     // method: "POST"
		/**** 选填方法 *****/
		postBefore(params){}, // 请求前
		postAfter(params){},  // 请求后
		postFinish(params){}, // 请求完成
	},
	uniMixin: "post",
}
```

### 自定义

```js
export default {
	data() {
		return {
			/**** 默认生成data *****/
			xxxState: {
				path: undefined,  // 必填项，请求路径
				params: undefined,
				data: undefined,
				loading: false,
				request: undefined
			}
		}
	},
	methods: {
		/**** 默认生成请求方法 *****/
		xxxRequest(params){},     // method: "GET"
		/**** 选填方法 *****/
		xxxBefore(params){},
		xxxAfter(params){},
		xxxFinish(params){},
	},
	uniMixin: "getXXX",  // 请求方式get关键字XXX
}
```

### 多请求

```js
export default {
	data(){
		return {
			/**** 默认生成data *****/
			getState: {
				path: undefined,  // 必填项，请求路径
				params: undefined,// 选填，请求参数
				data: undefined,  // 选填，请求结果
				loading: false,   // 不填，请求状态
				request: undefined// 不填，请求实例
			},
			postState: {
				path: undefined,  // 必填项，请求路径
				params: undefined,// 选填，请求参数
				data: undefined,  // 选填，请求结果
				loading: false,   // 不填，请求状态
				request: undefined// 不填，请求实例
			}
		}
	},
	methods:{
		/**** 默认生成请求方法 *****/
		getRequest(params){},     // method: "GET"
		postRequest(params){},     // method: "POST"
		/**** 选填方法 *****/
		getBefore(params){}, // 请求前
		getAfter(params){},  // 请求后
		getFinish(params){}, // 请求完成
		postBefore(params){},// 请求前
		postAfter(params){}, // 请求后
		postFinish(params){},// 请求完成
	},
	uniMixin: ["get", "post"],
}
```

## 对象方式
### get请求

```js
export default {
	data(){
		return {
			/**** 默认生成data *****/
			getState: {
				path: undefined,  // 必填项，请求路径
				params: undefined,
				data: undefined,
				loading: false,
				request: undefined
			}
		}
	},
	methods:{
		/**** 默认生成请求方法 *****/
		getRequest(params){},     // method: "GET"
		/**** 选填方法 *****/
		getBefore(params){},
		getAfter(params){},
		getFinish(params){},
	},
	uniMixin: {method:'GET'},
}
```

### post请求

```js
export default {
	data(){
		return {
			/**** 默认生成data *****/
			postState: {
				path: undefined,  // 必填项，请求路径
				params: undefined,
				data: undefined,
				loading: false,
				request: undefined
			}
		}
	},
	methods:{
		/**** 默认生成请求方法 *****/
		postRequest(params){},     // method: "POST"
		/**** 选填方法 *****/
		postBefore(params){},
		postAfter(params){},
		postFinish(params){},
	},
	uniMixin: {method:"POST"},
}
```

### 自定义方式

```js
export default {
	data() {
		return {
			/**** 默认生成data *****/
			xxxState: {
				path: undefined,  // 必填项，请求路径
				params: undefined,
				data: undefined,
				loading: false,
				request: undefined
			}
		}
	},
	methods: {
		/**** 默认生成请求方法 *****/
		xxxRequest(params){},     // method: "GET"
		/**** 选填方法 *****/
		xxxBefore(params){},
		xxxAfter(params){},
		xxxFinish(params){},
	},
	uniMixin: {
		method: 'GET',
		key: 'xxx',
	},
}
```

### 多请求

```js
export default {
	data(){
		return {
			/**** 默认生成data *****/
			getState: {
				path: undefined,  // 必填项，请求路径
				params: undefined,
				data: undefined,
				loading: false,
				request: undefined
			},
			postState: {
				path: undefined,  // 必填项，请求路径
				params: undefined,
				data: undefined,
				loading: false,
				request: undefined
			}
		}
	},
	methods:{
		/**** 默认生成请求方法 *****/
		getRequest(params){},     // method: "GET"
		postRequest(params){},     // method: "POST"
		/**** 选填方法 *****/
		getBefore(params){},
		getAfter(params){},
		getFinish(params){},
		postBefore(params){},
		postAfter(params){},
		postFinish(params){},
	},
	uniMixin: [{method:'GET'}, {method:"POST"}],
}
```

## 函数方式

```js
export default {
	data(){
		return {
			/**** 默认生成data *****/
			getState: {
				path: undefined,  // 必填项，请求路径
				params: undefined,
				data: undefined,
				loading: false,
				request: undefined
			}
		}
	},
	methods:{
		/**** 默认生成请求方法 *****/
		getRequest(params){},     // method: "GET"
		/**** 选填方法 *****/
		getBefore(params){},
		getAfter(params){},
		getFinish(params){},
	},
	uniMixin(){
		return {method:'GET'};   //返回参数列表
	},
}
```

## 参数
### method

请求方式，目前仅支持GET/POST。

### key

关键字字符串，默认同请求方式字段相同。

### debounce[1.1版本舍弃]

节流时间，单位毫秒。

### throttle[1.1版本舍弃]

防抖时间，单位毫秒。

## State
### path

必填项，请求路径。

### params

选填项，请求参数。

### data

选填项，响应结果。
 
### loading

不填，请求响应状态，默认为false。

### request

不填，请求实例。

## 方法
### xxxBefore

选填项，请求前方法。

* 有返回值，则替换[params](#params)请求参数。
* 返回Promise.reject()则终止请求

### xxxRequest

不要填写此项，请求发起/请求启动。

### xxxAfter

选填项，响应后方法。

* 有返回值，则替换[xxxRequest](#xxxRequest)的结果给[data](#data)响应结果
* 返回Promise.reject()则终止请求

### xxxFinish

选填项，响应完成方法。

# useIntercept
## 全局拦截
### 拦截

```js
import uniMixin,{useIntercept} from "@xyzi/unimixin";

useIntercept("before", function(params){
})
```

### 多拦截

自上而下执行

```js
import {useIntercept} from "@xyzi/unimixin";

useIntercept("before", function(params) {
})

useIntercept("before", function(params) {
})
```

### 手动清除拦截

```js
import {useIntercept} from "@xyzi/unimixin";

const exit = useIntercept("before", function(params) {
})
exit(); //清除拦截
```

## 拦截方式
### before

选填项，全局请求前方法。有返回值，则覆盖前一个params。

### after

选填项，全局响应后方法。有返回值，则覆盖前一个params。

### finish

选填项，全局响应完成方法。有返回值，则覆盖前一个params。

### xxxBefore

选填项，全局请求前方法。有返回值，则覆盖前一个params。

> 注意

* 用于局部页面时，必须在页面卸载前手动[手动清除拦截](#手动清除拦截)

### xxxAfter

选填项，全局响应后方法。有返回值，则覆盖前一个params。

> 注意

* 用于局部页面时，必须在页面卸载前手动[手动清除拦截](#手动清除拦截)

### xxxFinish

选填项，全局响应完成方法。有返回值，则覆盖前一个params。

> 注意

* 用于局部页面时，必须在页面卸载前手动[手动清除拦截](#手动清除拦截)

## 执行周期
### 请求-响应

```
全局before --> 全局xxxBefore --> 页面before --> 全局/默认useRequest
全局after  --> 页面after  --> 全局xxxAfter  --> State.data赋值  -->
页面finish --> 全局xxxFinish --> 全局finish
```

### 请求-异常

```
请求异常    --> 页面finish --> 全局xxxFinish --> 全局finish
```

# useRequest
## 自定义

```js
import {useRequest} from "@xyzi/unimixin";

useRequest(params => uni.request(params));
```

## 参数
### url

请求路径

### method

请求方式

### data

请求参数

### success

成功回调

### fail

失败回调

# Composition API

```js
import {useUniMixin} from "@xyzi/unimixin";
export default {
	setup() {
		const {
			xxxState,
			xxxRequest,
			xxxIntercept
		} = useUniMixin({
			method: "GET",
			key: "xxx",
			path: "/",  // 必填项, 请求路径
			params: undefined,
			data: undefined,
		});
		
		xxxIntercept('before',function(){})
		xxxIntercept('after',function(){})
		xxxIntercept('finish',function(){})
		
		return {
			xxxState,
			xxxRequest,
		}
	}
}
```

# Example
## 准备

* main.js

```js
import uniMixin from "@xyzi/unimixin";

Vue.use(uniMixin);
```

## uniMixin
### 字符串关键字

* get

```js
export default {
	data() {
		return {
			getState: {
				path: "/getList",
				params: {}
			}
		}
	},
	methods: {
		getAfter(res) {	}
	},
	uniMixin: "get",
}
```

* post

```js
export default {
	data() {
		return {
			postState: {
				path: "/save",
				params: {}
			}
		}
	},
	methods: {
		postAfter(res) {}
	},
	uniMixin: 'post',
}
```

### 自定义字符串关键字

```js
export default {
	data() {
		return {
			listState: {
				path: "/getList",
				params: {}
			}
		}
	},
	methods: {
		listAfter(res) { }
	},
	uniMixin: "getList",
}
```

### 多字符串关键字

```js
export default {
	data() {
		return {
			getState: {
				path: "/getList",
				params: {}
			},
			postState: {
				path: "/save",
				params: {}
			}
		}
	},
	methods: {
		getAfter(res) {},
		postAfter(res) {}
	},
	uniMixin: ["get","post"],
}
```

### 对象关键字

* get

```js
export default {
	data() {
		return {
			getState: {
				path: "/getList",
				params: {}
			}
		}
	},
	methods: {
		getAfter(res) {}
	},
	uniMixin: {method: "GET"},
}
```

* post

```js
export default {
	data() {
		return {
			postState: {
				path: "/save",
				params: {}
			}
		}
	},
	methods: {
		postAfter(res) {}
	},
	uniMixin: {method: "POST"},
}
```

### 自定义对象关键字

```js
export default {
	data() {
		return {
			listState: {
				path: "/getList",
				params: {}
			}
		}
	},
	methods: {
		listAfter(res) {}
	},
	uniMixin: {
		method: 'GET',
		key: 'list',
	},
}
```

### 多对象关键字

```js
export default {
	data() {
		return {
			getState: {
				path: "/getList",
				params: {}
			},
			postState: {
				path: "/save",
				params: {}
			}
		}
	},
	methods: {
		getAfter(res) {},
		postAfter(res) {}
	},
	uniMixin: [{method: "GET"},{method:'POST'}],
}
```

### 函数式

```js
export default {
	data() {
		return {
			getState: {
				path: "/getList",
				params: {}
			}
		}
	},
	methods: {
		getAfter(res) {}
	},
	uniMixin(){
		return {method: "GET"};
	},
}
```

## xxxBefore
### Params xxx

初始参数来源于[xxxRequest](#xxxRequest)

```js
export default {
	methods: {
		getBefore(params){
			//params come from xxx params
		}
	}
}
```

### Params Change

改变[params](#params)请求参数

```js
export default {
	methods: {
		getBefore(){
			//this.getState.params = ...
		}
	}
}
```

### Params Return

返回新请求参数，原有[params](#params)请求参数不变

```js
export default {
	methods: {
		getBefore(){
			//return 
		}
	}
}
```

### Reject Return

异步终止请求

```js
export default {
	methods: {
		getBefore(){
			// return Promise.reject();
		}
	}
}
```

### Promise Return

异步请求前置

```js
export default {
	methods: {
		getBefore(){
			//return new Promise((resolve,reject)=>...);
		}
	}
}
```

## xxxAfter
### Data Change

修改结果集[data](#data)

```js
export default{
	methods: {
		getAfter(res){
			// response data is automatically set to xxxData.
		}
	}
}
```

### Data Return

返回新结果集[data](#data)

```js
export default{
	methods: {
		getAfter(res){
			//return res;
		}
	}
}
```

### Reject Return

异步终止请求

```js
export default {
	methods: {
		getAfter(res){
			// return Promise.reject();
		}
	}
}
```

### Promise Return

异步请求

```js
export default {
	methods: {
		getAfter(res){
			// return new Promise((resolve,reject)=> ...);
		}
	}
}
```

## xxxFinish

```js
export default {
	methods: {
		getFinish(){
			//finish request
		}
	}
}
```